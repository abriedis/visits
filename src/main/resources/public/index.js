function registerVisit(url) {
    var visitorName = $("#visitorName").val();
    $.ajax({
        url: url,
        headers: {
            'Content-Type': 'application/json'
        },
        method: "POST",
        data: JSON.stringify({name: visitorName})
    }).then(() => log());
}

function registerIn() {
    registerVisit("/in");
}

function registerOut() {
    registerVisit("/out");
}

function log() {
    $.ajax({url: "/logs"}).then(data => {
        $("#visits").empty();
        data.forEach(item => {
            var node = document.createElement("div");
            var text = `${item.when} - ${item.who} - ${item.direction}`;
            var textnode = document.createTextNode(text);
            node.appendChild(textnode);
            $('#visits').append(node);
        })
    })
}
