package siva;

import java.util.List;
import org.springframework.data.repository.CrudRepository;

public interface VisitLogRepository extends CrudRepository<VisitLog, Long>{
    List<VisitLog> findAll();
}
