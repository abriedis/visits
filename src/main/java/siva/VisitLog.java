package siva;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class VisitLog {
    public enum Direction {
        IN,
        OUT
    }
    
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long id;
    
    @Column
    private String who;
    
    @Column
    private Date time;
    
    @Column
    @Enumerated(EnumType.STRING)
    private Direction direction;

    public VisitLog(String who, Date when, Direction direction) {
        this.who = who;
        this.time = when;
        this.direction = direction;
    }

    public VisitLog() {
    }

    public String getWho() {
        return who;
    }

    public Date getTime() {
        return time;
    }

    public Direction getDirection() {
        return direction;
    }
}