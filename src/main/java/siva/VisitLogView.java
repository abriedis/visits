package siva;

public class VisitLogView {
    public final String when;
    public final String who;
    public final String direction;

    public VisitLogView(VisitLog visit) {
        this.when = visit.getTime().toString();
        this.who = visit.getWho();
        this.direction = visit.getDirection().toString();
    }
}
