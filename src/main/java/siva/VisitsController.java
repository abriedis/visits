package siva;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class VisitsController {
    private final VisitLogRepository repository;

    public VisitsController(VisitLogRepository repository) {
        this.repository = repository;
    }
    
    @PostMapping("/in")
    public void in(@RequestBody VisitForm form) {
        registerVisit(form, VisitLog.Direction.IN);
    }

    private void registerVisit(VisitForm form, VisitLog.Direction direction) {
        VisitLog visit = new VisitLog(
                form.name,
                new Date(),
                direction
        );
        repository.save(visit);
    }

    @PostMapping("/out")
    public void out(@RequestBody VisitForm form) {
        registerVisit(form, VisitLog.Direction.OUT);
    }

    @GetMapping("/logs")
    public List<VisitLogView> logs() {
        return repository
                .findAll()
                .stream()
                .map(VisitLogView::new)
                .collect(Collectors.toList());
    }
}
